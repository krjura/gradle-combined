# Purpose #
Purpose of this plugin is to decrease css and js loading times of a Spring Boot application based on Thymeleaf.

Web projects have become very complicated when it comes to the amount of JavaScript and CSS files required. Main reasons behind this is that developers use more external dependencies in their projects. Most modern web browsers have limitations with regards to the amount of concurrent connections they can use. This number usually ranges from 6-9 concurrent connections. If you have more JavaScript and CSS files then this browsers will wait until previous requests are done before sending requests for other files. 

# Quick Start #

In order to use this gradle plugin in your project you must first download it locally and build it. I will provide a valid repository later on. After you build it and make a maven artifact add the following code to your *build.gradle*

~~~~
buildscript {
    repositories {
        mavenLocal()
    }

    dependencies {
        classpath 'biz.db.dev.gradle.plugins:combined:0.1'
    }
}

apply plugin: 'biz.db.dev.gradle.plugins.combined'
~~~~

##combining css##
In order to combine css you will have to define what needs to be combined, how and where. The following code example shows basic elements of the configuration:

~~~~
combineCss {

    sourceSet {
        primary 'web/static/css/primary_1.css'
        primary 'web/static/css/primary_2.css'

        combine 'web/static/css/combine_1.css'
        combine 'web/static/css/combine_2.css'

        secondary 'web/static/css/secondary_1.css'
        secondary 'web/static/css/secondary_2.css'

        target 'web/static/combined/css/app.dep.combined.css'
        compressTarget true
        applyVersion true

        staticRoot 'templates'
        webRoot '/'
    }

    htmlImportFile 'templates/web/html/import-css.html'
}
~~~~

The following elements can be used:

* **sourceSet** - is a configuration element for one combined file. This configuration element can be repeated for each combined file.
* **primary** - (**optional**) - primary is a file which should not be combined with other files yet. This is useful if you have some main application file which needs to be loaded first and you are still working on this peace of code. If you add this file to the combined file you would have to build it every time you change it. If you use this element, file will be added as is so reloading you browser will update the file as well. This element can be repeated.
* **combine** - is used to define files which should be combined. This element can be repeated.
* **secondary** - (**optional**) - has the same purpose as the primary except that this file will be placed last in this set.
* **staticRoot** - is the root path of the target file and all primary, combined and secondary files. Path should be relative to the project using the plugin.
* **target** - is the path of the combined file. During combine process value of *staticRoot + File.separator* will be prepended.
* **compressTarget** - true|false - (**optional** - default false) - will create and additional gziped version of the target file. This is usefull if you want to serve already compressed file to save on CPU.
* **applyVersion** - true|false - (**optional** - default false) - will add version string before the extension. Version string is the time of running the task in the format "yyyyMMddHHmmss".
* **webRoot** - is the path target file is exposed at on your web server. When generating html import file each target file will be prepended with *webRoot + /*.
* **htmlImportFile** - is the location where html import file will be stored. File will be prepended with *staticRoot + File.separator*.

##combining js##
Configuration of combining js files is the same as with css with exception of different configuration name. The following code shows and example of js configuration:

~~~~
combineJs {

    sourceSet {
        primary 'web/static/js/primary_1.js'
        primary 'web/static/js/primary_2.js'

        combine 'web/static/js/combine_1.js'
        combine 'web/static/js/combine_2.js'

        secondary 'web/static/js/secondary_1.js'
        secondary 'web/static/js/secondary_2.js'

        target 'web/static/combined/js/app.dep.combined.js'
        compressTarget true
        applyVersion true

        staticRoot 'templates'
        webRoot '/'
    }

    htmlImportFile 'templates/web/html/import-js.html'
}
~~~~