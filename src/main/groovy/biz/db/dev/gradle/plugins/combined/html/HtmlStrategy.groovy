package biz.db.dev.gradle.plugins.combined.html

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet

public interface HtmlStrategy {

    void create(String htmlImportFile);

    void writeHeader();

    void writeFooter();

    void writeTargetEntry(ConfigurationSourceSet sourceSet);

    void writePrimaryEntry(ConfigurationSourceSet sourceSet, String primary);

    void writePrimaryEntries(ConfigurationSourceSet sourceSet);

    void writeSecondaryEntry(ConfigurationSourceSet sourceSet, String secondary);

    void writeSecondaryEntries(ConfigurationSourceSet sourceSet);

    void writeEntry(String webRoot, String entry);
}
