package biz.db.dev.gradle.plugins.combined.js

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet
import biz.db.dev.gradle.plugins.combined.utils.PluginUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat
import java.util.zip.GZIPOutputStream

public class TargetStrategyImpl implements TargetStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(TargetStrategyImpl.class)

    private File targetFile;

    private Writer targetWriter;

    public TargetStrategyImpl(String targetFilename, boolean applyVersion) {
        this.targetFile = new File( targetFilename );
        this.targetWriter = targetFile.newWriter(false);
    }

    public void writeCombine(ConfigurationSourceSet sourceSet) {
        sourceSet.combines.each { combine ->
            write(sourceSet.staticRoot, combine)
        }
    }

    public void write(String staticRoot, String source) {
        String sourceFilename = staticRoot + File.separator + source

        File sourceFile = new File( sourceFilename );

        if( ! sourceFile.exists() ) {
            LOG.warn("${sourceFilename} does not exists. Skipping it");
            return;
        }

        // Get a reader for the input file
        sourceFile.withReader { sourceFileReader ->

            // And write data from the input into the output
            targetWriter.append("\n/*\n")
            targetWriter.append("*************************************************").append("\n")
            targetWriter.append("file: " + sourceFilename).append("\n")
            targetWriter.append("*************************************************").append("\n")
            targetWriter.append("*/\n")

            targetWriter << sourceFileReader << '\n'
        }
    }

    public void gzip() {
        String gzipFile = this.targetFile.getAbsolutePath() + ".gz";

        GZIPOutputStream gzos;
        FileInputStream fin;
        try {
            gzos = new GZIPOutputStream(new FileOutputStream(gzipFile));

            fin = new FileInputStream(this.targetFile);

            byte[] buffer = new byte[1024];

            int len;
            while ((len = fin.read(buffer)) > 0) {
                gzos.write(buffer, 0, len);
            }

            fin.close();

            gzos.finish();
        } catch ( IOException ex ) {
            PluginUtils.close(fin);
            PluginUtils.close(gzos);

            LOG.warn("Cannot gzip file: {}", this.targetFile.getAbsolutePath());
        }
    }

    public void close() {
        PluginUtils.close(this.targetWriter);
    }
}
