package biz.db.dev.gradle.plugins.combined.js

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet

public class NoOpTargetStrategy implements TargetStrategy {

    @Override
    void writeCombine(ConfigurationSourceSet sourceSet) {
        // ignore
    }

    @Override
    void write(String staticRoot, String source) {
        // ignore
    }

    @Override
    void gzip() {
        // ignore
    }

    @Override
    void close() {
        // ignore
    }
}
