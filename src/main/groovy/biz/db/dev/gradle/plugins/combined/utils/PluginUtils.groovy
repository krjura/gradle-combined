package biz.db.dev.gradle.plugins.combined.utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory;

public class PluginUtils {

    private static final Logger LOG = LoggerFactory.getLogger(PluginUtils.class)

    public static final void close(Writer writer) {
        if( writer == null) {
            return;
        }

        try {
            writer.flush();
            writer.close();
        } catch ( Exception ex ) {
            LOG.warn("cannot close writter", ex)
        }
    }

    public static final void close(OutputStream outputStream) {
        if( outputStream == null) {
            return;
        }

        try {
            outputStream.flush();
            outputStream.close();
        } catch ( Exception ex ) {
            LOG.warn("cannot close outputStream", ex)
        }
    }

    public static final void close(InputStream inputStream) {
        if( inputStream == null) {
            return;
        }

        try {
            inputStream.close();
        } catch ( Exception ex ) {
            LOG.warn("cannot close inputStream", ex)
        }
    }
}
