package biz.db.dev.gradle.plugins.combined.js

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet

public interface TargetStrategy {

    void writeCombine(ConfigurationSourceSet sourceSet);

    void write(String staticRoot, String source);

    void gzip();

    void close();
}
