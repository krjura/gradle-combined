package biz.db.dev.gradle.plugins.combined.tasks

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet
import biz.db.dev.gradle.plugins.combined.html.HtmlStrategy
import biz.db.dev.gradle.plugins.combined.js.TargetStrategy
import org.gradle.api.DefaultTask
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.SimpleDateFormat

public abstract class AbstractCombinedTask extends DefaultTask {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractCombinedTask.class);

    private static final SimpleDateFormat VERSION_TAG = new SimpleDateFormat("yyyyMMddHHmmss")

    protected abstract TargetStrategy resolveTargetStrategy(ConfigurationSourceSet sourceSet);

    protected HtmlStrategy htmlStrategy;

    protected String version(String targetFilename, boolean applyVersion) {

        // version is not needed
        if( ! applyVersion ) {
            return targetFilename;
        }

        int index = targetFilename.lastIndexOf('.');

        if( index == -1) {
            return targetFilename;
        }

        return targetFilename.substring(0, index) + "-" + VERSION_TAG.format(new Date()) + "." + targetFilename.substring(index + 1); ;
    }

    protected void processSourceSet(ConfigurationSourceSet sourceSet) {

        TargetStrategy targetStrategy;

        try {
            targetStrategy = resolveTargetStrategy(sourceSet);

            writeSourceSet(sourceSet, targetStrategy);
        } catch ( IOException ex ) {
            LOG.warn("Cannot save source set", ex);
        } finally {
            targetStrategy.close();
        }

        if( sourceSet.compressTarget ) {
            targetStrategy.gzip()
        }
    }

    protected void writeSourceSet(ConfigurationSourceSet sourceSet, TargetStrategy targetStrategy) { ;

        // write all primaries
        this.htmlStrategy.writePrimaryEntries(sourceSet);

        // combine
        targetStrategy.writeCombine(sourceSet);

        // then write target to html file
        this.htmlStrategy.writeTargetEntry(sourceSet)

        // write all secondaries
        this.htmlStrategy.writeSecondaryEntries(sourceSet);
    }
}
