package biz.db.dev.gradle.plugins.combined

import biz.db.dev.gradle.plugins.combined.tasks.CombineCssTask
import biz.db.dev.gradle.plugins.combined.tasks.CombineJsTask
import org.gradle.api.Plugin
import org.gradle.api.Project

public class CombinedPlugin implements Plugin<Project> {

    void applyTasks(Project project) {
        project.task('combineJs', type: CombineJsTask, group: 'Combine', description: 'Combine multiple javascript files into one or more') {}
        project.task('combineCss', type: CombineCssTask, group: 'Combine', description: 'Combine multiple css files into one or more') {}
    }

    void apply(Project project) {

        project.extensions.create("combineJs", Configuration)
        project.extensions.create("combineCss", Configuration)

        applyTasks(project);
    }
}