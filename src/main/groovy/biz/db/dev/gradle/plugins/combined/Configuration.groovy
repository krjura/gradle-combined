package biz.db.dev.gradle.plugins.combined

import org.gradle.util.ConfigureUtil

class Configuration {

    List<ConfigurationSourceSet> sourceSets;

    String htmlImportFile;

    public Configuration() {
        this.sourceSets = new LinkedList<>();
    }

    void sourceSet(Closure closure) {

        ConfigurationSourceSet sourceSet = new ConfigurationSourceSet();

        ConfigureUtil.configure(closure, sourceSet)

        this.sourceSets.add(sourceSet);
        this.htmlImportFile = '';
    }

    void htmlImportFile(String htmlImportFile) {
        this.htmlImportFile = htmlImportFile;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "sourceSets=" + sourceSets +
                ", htmlImportFile='" + htmlImportFile + '\'' +
                '}';
    }
}