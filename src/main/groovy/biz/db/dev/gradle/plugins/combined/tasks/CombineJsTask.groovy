package biz.db.dev.gradle.plugins.combined.tasks

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet
import biz.db.dev.gradle.plugins.combined.html.HtmlStrategy
import biz.db.dev.gradle.plugins.combined.html.ThymeleafJsHtmlStrategy
import biz.db.dev.gradle.plugins.combined.js.NoOpTargetStrategy
import biz.db.dev.gradle.plugins.combined.js.TargetStrategy
import biz.db.dev.gradle.plugins.combined.js.TargetStrategyImpl
import org.gradle.api.tasks.TaskAction
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CombineJsTask extends AbstractCombinedTask {

    private static final Logger LOG = LoggerFactory.getLogger(CombineJsTask.class);

    protected HtmlStrategy createHtmlStrategy() {
        this.htmlStrategy = new ThymeleafJsHtmlStrategy();

        this.htmlStrategy.create(project.combineJs.htmlImportFile);
        this.htmlStrategy.writeHeader();

        return this.htmlStrategy;
    }

    protected TargetStrategy resolveTargetStrategy(ConfigurationSourceSet sourceSet) {
        if( sourceSet.staticRoot == null || sourceSet.target == null ) {
            return new NoOpTargetStrategy();
        } else {

            // update target with version if needed
            String versionedTarget = version(sourceSet.target, sourceSet.applyVersion);
            sourceSet.target = versionedTarget;

            def targetFilename = sourceSet.staticRoot + File.separator + sourceSet.target;
            return new TargetStrategyImpl(targetFilename, sourceSet.applyVersion);
        }
    }

    @TaskAction
    def combineJs() {
        createHtmlStrategy();

        for( ConfigurationSourceSet sourceSet : project.combineJs.sourceSets ) {
            processSourceSet(sourceSet);
        }

        this.htmlStrategy.writeFooter()
    }
}
