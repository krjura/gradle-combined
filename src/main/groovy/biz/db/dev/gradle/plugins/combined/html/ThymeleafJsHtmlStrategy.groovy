package biz.db.dev.gradle.plugins.combined.html

import biz.db.dev.gradle.plugins.combined.ConfigurationSourceSet

public class ThymeleafJsHtmlStrategy implements HtmlStrategy {

    private File htmlImportFile;

    public void create(String htmlImportFile) {
        // prepare html import file writer
        this.htmlImportFile = new File(htmlImportFile);

        if( this.htmlImportFile.exists()) {
            this.htmlImportFile.delete();
        }
    }

    public void writeHeader() {
        this.htmlImportFile.append "<th:block th:fragment='combined-scripts'>"
        this.htmlImportFile.append "\n"
    }

    public void writeFooter() {
        this.htmlImportFile.append "</th:block>"
    }

    public void writeTargetEntry(ConfigurationSourceSet sourceSet) {
        if( sourceSet.target == null ) {
            return;
        }

        this.htmlImportFile.append "\t<script src='#' th:src='@{${sourceSet.webRoot}${sourceSet.target}}'></script>"
        this.htmlImportFile.append "\n"
    }

    public void writePrimaryEntry(ConfigurationSourceSet sourceSet, String primary) {
        writeEntry(sourceSet.webRoot, primary);
    }

    public void writePrimaryEntries(ConfigurationSourceSet sourceSet) {
        sourceSet.primaries.each { primary ->
            writeEntry(sourceSet.webRoot, primary);
        }
    }

    public void writeSecondaryEntry(ConfigurationSourceSet sourceSet, String secondary) {
        writeEntry(sourceSet.webRoot, secondary);
    }

    public void writeSecondaryEntries(ConfigurationSourceSet sourceSet) {
        sourceSet.secondaries.each { secondary ->
            writeEntry(sourceSet.webRoot, secondary);
        }
    }

    public void writeEntry(String webRoot, String entry) {
        htmlImportFile.append "\t<script src='#' th:src='@{${webRoot}${entry}}'></script>"
        htmlImportFile.append "\n"
    }
}
