package biz.db.dev.gradle.plugins.combined

public class ConfigurationSourceSet {

    List<String> combines;

    List<String> primaries;

    List<String> secondaries;

    String target;

    boolean compressTarget;

    boolean applyVersion;

    String staticRoot;

    String webRoot;

    ConfigurationSourceSet() {
        this.combines = new LinkedList<>();
        this.primaries = new LinkedList<>();
        this.secondaries = new LinkedList<>();
        this.staticRoot = '';
        this.webRoot = '/';

        this.compressTarget = false;
        this.applyVersion = false;
    }

    void combine(String combine) {
        this.combines.add(combine);
    }

    void primary(String primary) {
        this.primaries.add(primary);
    }

    void secondary(String secondary) {
        this.secondaries.add(secondary);
    }

    void target(String target) {
        this.target = target;
    }

    void staticRoot(String staticRoot) {
        this.staticRoot = staticRoot;
    }

    void webRoot(String webRoot) {
        this.webRoot = webRoot;
    }

    void compressTarget(boolean compressTarget) {
        this.compressTarget = compressTarget;
    }

    void applyVersion(boolean applyVersion) {
        this.applyVersion = applyVersion;
    }

    @Override
    public String toString() {
        return "ConfigurationSourceSet{" +
                "combines=" + combines +
                ", primaries=" + primaries +
                ", secondaries=" + secondaries +
                ", target='" + target + '\'' +
                ", staticRoot='" + staticRoot + '\'' +
                ", webRoot='" + webRoot + '\'' +
                ", compressTarget='" + compressTarget + '\'' +
                ", applyVersion='" + applyVersion + '\'' +
                '}';
    }
}
